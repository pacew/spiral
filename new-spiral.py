import sys, os
import mido
import math
import numpy as np
import time
import cairo
import colorsys

time_limit = 300

declared_bpm = 112


midi_filename = 'example.mid'
width = 500
height = 500
center_x = width / 2
center_y = height / 2

symbol_width = 8.0
symbol_spacing = 18.0


frames_per_second = 24

NUM_NOTES = 128

tracks = []

def lscale(val, from_left, from_right, to_left, to_right):
    if from_left == from_right:
        return to_right

    return ((val - from_left) 
            / (from_right - from_left) 
            * (to_right - to_left)
            + to_left)

def lscale_clamp(val, from_left, from_right, to_left, to_right):
    if from_left == from_right:
        return to_right

    x = ((val - from_left)
         / (from_right - from_left)
         * (to_right - to_left)
         + to_left)
    if to_left < to_right:
        if x < to_left:
            x = to_left
        if x > to_right:
            x = to_right
    else:
        if x < to_right:
            x = to_right
        if x > to_left:
            x = to_left
    return x


def dtor(val):
    return val / 180.0 * math.pi



class Track:
    def __init__(self, filename, hue, offset, decay):
        self.notes = {}
        self.filename = filename
        self.hue = hue
        self.offset = offset
        self.decay = decay
        self.events = []
        self.event_idx = 0

        factor = 120 / declared_bpm

        with mido.MidiFile(filename) as inf:
            t = 0
            for msg in inf:
                delta = msg.time * factor
                t += delta
                self.events.append([t, msg])

    def finished(self):
        return self.event_idx >= len(self.events)

    def first_note(self):
        idx = 0
        for t, msg in self.events:
            if msg.type == 'note_on' and msg.velocity > 0:
                return t
        return None

    def process_until(self, secs):
        while not self.finished():
            (t, msg) = self.events[self.event_idx]
            if t > secs:
                break

            if msg.type == 'note_on' and (msg.channel+1) != 10:
                note_num = msg.note + self.offset
                if msg.velocity > 0:
                    self.strike(note_num)
                else:
                    self.release(note_num, t)
                    
            elif msg.type == 'note_off' and (msg.channel+1) != 10:
                note_num = msg.note + self.offset
                self.release(note_num, t)

            self.event_idx += 1

        return self.finished()

    def strike(self, note_num):
        try:
            note = self.notes[note_num]
        except KeyError:
            note = Note(note_num, self.hue, self.decay)
            self.notes[note_num] = note
        note.strike()

    def release(self, note_num, secs):
        try:
            note = self.notes[note_num]
        except KeyError:
            return
        note.release(secs)

    def draw(self, ctx, secs):
        for note_num, note in self.notes.items():
            note.draw(ctx, secs)


# note_num can be a real number to get positions on the spiral 
def note_position(note_num):
    r = ((127 - note_num) / 12.0) * symbol_spacing + 10.0
    theta = -(127 - note_num) / 12.0 * dtor(360) + dtor(90) + dtor(360) / 12
    x = r * np.cos(theta) + center_x
    y = r * np.sin(theta) + center_y
    return x, y


class Note:
    def __init__(self, note_num, hue, decay):
        self.note_num = note_num
        self.hue = hue
        self.decay = decay
        self.on = False
        self.release_time = 0.0
        self.x, self.y = note_position(note_num)

    def strike(self):
        self.on = True

    def release(self, secs):
        self.on = False
        self.release_time = secs

    def age(self, secs):
        if self.on:
            return 0.0
        else:
            return secs - self.release_time

    def draw(self, ctx, secs):
        v = lscale_clamp(self.age(secs), 0, self.decay, 100, 0)
        r, g, b = colorsys.hsv_to_rgb(self.hue / 360.0, 100, v)
        age = self.age(secs)
        alpha = lscale_clamp(age, 0, self.decay, 1, 0)
        ctx.set_source_rgba(r, g, b, alpha)
        ctx.arc(self.x, self.y, symbol_width, 0, dtor(360))
        ctx.fill()


def draw(frame_num):
    sur = cairo.ImageSurface(cairo.FORMAT_ARGB32, width, height)
    ctx = cairo.Context(sur)

    ctx.set_source_rgb(0, 0, 0)
    ctx.rectangle(0, 0, width, height)
    ctx.fill()
    
    ctx.set_source_rgb(0.1, 0.1, 0.1)
    
    subdivide = 4
    for step in range(NUM_NOTES * subdivide):
        note_num = step / subdivide
        x, y = note_position(note_num)
        if step == 0:
            ctx.move_to(x, y)
        else:
            ctx.line_to(x, y)
    ctx.stroke()
    
    for note_num in range(NUM_NOTES):
        ctx.set_source_rgb(.2, .2, .2)

        x, y = note_position(note_num)
        ctx.arc(x, y, symbol_width * 1.2, 0, dtor(360))
        ctx.stroke()

    secs = frame_num / frames_per_second

    for track in tracks:
        track.process_until(secs)

    for track in tracks:
        track.draw(ctx, secs)

    fname = f'frames/f{frame_num:04d}.png'
    sur.write_to_png(fname)

def finished(frame_num):
    secs = frame_num / frames_per_second
    if secs > time_limit:
        return True

    still_working = False
    for track in tracks:
        if not track.finished():
            still_working = True
    return not still_working

def get_hue(tnum):
    return 20 + tnum * 20

# hue offset decay
if False:
    tracks.append(Track('Take22_Cardinal Synth-2.mid', get_hue(0), 0, 1.2))
    tracks.append(Track('Take22_Cardinal Synth-5.mid', get_hue(1), 12, 1.2))
    tracks.append(Track('Take22_Vitalium-2.mid', get_hue(2), 24, 0.7))
    tracks.append(Track('Take22_Vitalium-3.mid', get_hue(3), 48, 0.7))
else:
    tracks.append(Track('mylofy/Consolidated-epiano-1.mid', get_hue(0), 0, 0.5))
    tracks.append(Track('mylofy/Consolidated-Ld-1-t1-1.mid', get_hue(1), 0, 0.5))
    tracks.append(Track('mylofy/Consolidated-Pad-1-t1-1.mid', get_hue(2), 0, 0.5))
    tracks.append(Track('mylofy/Consolidated-Rhodes-1-t1-1.mid', get_hue(3), 0, 0.5))
    tracks.append(Track('mylofy/Consolidated-SynthBass-1-t1-1.mid', get_hue(4), 0, 0.5))
    tracks.append(Track('mylofy/Consolidated-Vitalium-1.mid', get_hue(5), 0, 0.5))

first_note_time = min([t.first_note() for t in tracks])
print('first note', first_note_time)
first_note_time = 0

frame_num = 0
while not finished(frame_num):
    frame_num += 1
    draw(frame_num)
