import sys, os
import mido
import math
import numpy as np
import time
import cairo
import colorsys

midi_filename = 'example.mid'
width = 500
height = width
seconds_per_frame = 1.0/24.4
output_secs = 0
frame_number = 0

NUM_NOTES = 128

note_hsv = []
note_brightness = []
for i in range(NUM_NOTES):
    note_hsv.append([0, 0, 0].copy())
    note_brightness.append(0)


def add_absolute_times_to_midi(midifile_in):
    output_list = []
    msg_count = 0
    current_time = 0
    for msg in midifile_in:
        current_time += msg.time
        output_list.append( [ current_time,msg])

        msg_count += 1
    return output_list


def channel_num_to_hue(channel_num):
    return np.mod(channel_num*20.0, 360)


def draw_single_note(note_num, ctx, x_pos, y_pos, symbol_width, note_status, note_last_on_time):
    if len(note_status) > 0:
        # Note is on (and has colour based on channel number)
        for note in note_status:
            # 360, 100, 100
            h = channel_num_to_hue(note.channel)
            s = 100
            v = np.clip(note.velocity/128.0*80.0*2+20.0, 0, 100)
            note_hsv[note_num] = (h, s, v)
            note_brightness[note_num] = 1
            
            r, g, b = colorsys.hsv_to_rgb(note_hsv[note_num][0],
                                          note_hsv[note_num][1],
                                          note_hsv[note_num][2])
            ctx.set_source_rgba(r, g, b, 1)
            ctx.arc(x_pos, y_pos, symbol_width, 0, dtor(360))
            ctx.fill()
    else:
        # Note is off and possibly fading:
        note_age = output_secs - note_last_on_time
        fade_duration = 1.0 # seconds
        note_brightness[note_num] = np.clip( (fade_duration-note_age)/fade_duration, 0, 1.0)

        r, g, b = colorsys.hsv_to_rgb(note_hsv[note_num][0],
                                      note_hsv[note_num][1],
                                      note_hsv[note_num][2])
        ctx.set_source_rgba(r, g, b, note_brightness[note_num])
        ctx.arc(x_pos, y_pos, symbol_width, 0, dtor(360))
        ctx.fill()


def dtor(val):
    return val / 180.0 * math.pi


def update_display_spiral(skip_unchanged_notes=True):
    sur = cairo.ImageSurface(cairo.FORMAT_ARGB32, width, height)
    ctx = cairo.Context(sur)

    symbol_width = 8.0
    symbol_spacing = 18.0
    cx, cy = [width * 0.5, height * 0.5]

    ctx.set_source_rgb(0, 0, 0)
    ctx.rectangle(0, 0, width, height)
    ctx.fill()
    
    ctx.set_source_rgb(0.5, 0.5, 0.5)
    
    for n in range(0, 127*4):
        r = ((127*4 - n) / 12.0)/4 * symbol_spacing + 10.0
        theta = -(127*4 - n) /4/12.0 * dtor(360) + dtor(90) + dtor(360) / 12
        x_pos = r * np.cos(theta) + cx
        y_pos = r * np.sin(theta) + cy
        if n == 0:
            ctx.move_to(x_pos, y_pos)
        else:
            ctx.line_to(x_pos, y_pos)
    ctx.stroke()
    
    for n in range(0, 127):
        if note_update[n] or skip_unchanged_notes is False:
            note_update[n] = False
            r = ((127-n)/12.0) * symbol_spacing+10.0
            theta = -(127-n)/12.0 * np.pi * 2.0 + np.pi/2.0 +2*np.pi/12
            x_pos = r * np.cos(theta) + cx
            y_pos = r * np.sin(theta) + cy

            ctx.set_source_rgb(.6, .6, .6)
            ctx.arc(x_pos, y_pos, symbol_width * 1.2, 0, dtor(360))
            ctx.stroke()

            draw_single_note(n,
                             ctx, 
                             x_pos, y_pos, 
                             symbol_width,
                             note_status[n],
                             note_last_on_time[n])


    global frame_number
    frame_number += 1
    fname = f'frames/f{frame_number:06d}.png'
    print(fname)
    sur.write_to_png(fname)




midifile = mido.MidiFile(midi_filename)
absolute_midi_notes_list = add_absolute_times_to_midi(midifile)


note_status = [[]]*128
note_update = [True]*128
note_last_on_time = [-10000.0]*128


input_idx = 0
time_limit = 100
while input_idx < len(absolute_midi_notes_list) and output_secs < time_limit:
    output_secs += seconds_per_frame
    while input_idx < len(absolute_midi_notes_list) and absolute_midi_notes_list[input_idx][0] <= output_secs:
        msg = absolute_midi_notes_list[input_idx][1]
        input_idx += 1

        print(msg)

        if msg.type == 'note_on' and (msg.channel+1) != 10:
            note_num = msg.note
            note_update[note_num] = True
            if msg.velocity == 0:
                # velocity 0 is another way to turn off a note.
                note_status[note_num] = [x for x in note_status[note_num] if x.channel != msg.channel]
            else:
                note_status[note_num] = note_status[note_num] + [msg]
                note_last_on_time[note_num] = output_secs
        elif msg.type == 'note_off' and (msg.channel+1) != 10:
            note_update[msg.note] = True
            note_status[msg.note] = [x for x in note_status[msg.note] if x.channel != msg.channel]

    update_display_spiral(skip_unchanged_notes=False)
